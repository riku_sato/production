#include "PlayScene.h"
#include "Player.h"
#include "Stage.h"
#include "Enemy.h"
#include "Engine/GameObject/Camera.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	//プレイシーンに必要なもの
	CreateGameObject<Player>(this);
	CreateGameObject<Enemy>(this);
	CreateGameObject<Stage>(this);

	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(10, 20, 5));
	pCamera->SetTarget(D3DXVECTOR3(0, 0, 5));

}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}