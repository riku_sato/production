#include "Enemy.h"
#include "Player.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"),hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Enemy.fbx");
	assert(hModel_ >= 0);

	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, -0.1), D3DXVECTOR3(1.2, 1, 2));
	AddCollider(collision);


	position_.x = 0;
	position_.y = 0;
	position_.z = 15;

	
}

//更新
void Enemy::Update()
{
	//自分がやられたらゲームオーバーシーンへ
	if (FindObject("Player") == nullptr)
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//プレイヤーの弾に当たった時
//敵が消える
void Enemy::OnCollision(IGameObject * pTargert)
{
	if (pTargert->GetObjectName() == "Bullet")
	{
		KillMe();
	}
}