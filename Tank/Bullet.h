#pragma once
#include "Engine/GameObject/GameObject.h"

//トーラスを管理するクラス
class Bullet : public IGameObject
{
	//モデル番号
	int hModel_;

	//HLSLから作成されたシェーダーを入れる
	LPD3DXEFFECT	pEffect_;


	LPDIRECT3DTEXTURE9 pToonTex_;

	D3DXVECTOR3 bulletSpeed_;	//弾の速度
	D3DXVECTOR3 reflect_;


public:
	//コンストラクタ
	Bullet(IGameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	//弾の速度と向きの宣言
	void SetBulletSpeed(D3DXVECTOR3 bulletSpeed) { bulletSpeed_ = bulletSpeed; }

	// out : 正規化反射ベクトル（戻り値）
	// front : 進行ベクトル
	// normal: 衝突点での法線ベクトル
	//壁に向かった進行ベクトルを
	//衝突点の時の中心点から二倍にし
	//壁に平行な線を引きそこから
	//ベクトルの終点から平行な線に向かい
	//交わった点から同じ分伸ばしその終点に向かって
	//衝突点から線を伸ばしたのが反射ベクトル
	void calcReflectVector(D3DXVECTOR3& out, const D3DXVECTOR3& front, const D3DXVECTOR3& normal);

};