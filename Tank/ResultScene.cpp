#include "ResultScene.h"
#include "Engine/GameObject/GameObject.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene"), hPict_(-1)
{
}

//初期化
void ResultScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/GameClear.jpg");
	assert(hPict_ >= 0);
}

//更新
void ResultScene::Update()
{
	//エンターが押されたらプレイシーンに戻る(仮)
	if (Input::IsKeyDown(DIK_RETURN))

	{
		//何らかの処理
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void ResultScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void ResultScene::Release()
{
}