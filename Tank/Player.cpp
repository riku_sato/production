#include "Player.h"
#include "Bullet.h"
#include "Engine/DirectX/Direct3D.h"
#include "Engine/GameObject/Camera.h"
#include "Engine/ResouceManager/Model.h"


//発射位置初期化
const D3DXVECTOR3 FIRING_POINT = D3DXVECTOR3(0, 0, 0.5f);
const D3DXVECTOR3 END_POINT = D3DXVECTOR3(0, 0, 0);



//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//プレイヤーモデルロード
	hModel_ = Model::Load("data/Player.fbx");
	assert(hModel_ >= 0);

	position_.x = 5;
	position_.z = -5;

	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0, -0.3), D3DXVECTOR3(1.2, 1, 1.8));
	AddCollider(collision);

}

//更新
void Player::Update()
{
	//自分の位置を記憶
	prePos_ = position_;

	//回転
	if (Input::IsKey(DIK_RIGHT))
	{
		rotate_.y += 1;
	}

	if (Input::IsKey(DIK_LEFT))
	{
		rotate_.y -= 1;
	}

	
	//回転行列
	D3DXVECTOR3 move(0, 0, 0.125f);
	D3DXMATRIX m;
	// プレイヤーに合わせて回転する
	D3DXMatrixRotationY(&m, D3DXToRadian(rotate_.y));
	// 変数moveを行列で変形し、moveに再代入する
	D3DXVec3TransformCoord(&move, &move, &m);


	//移動操作
	if (Input::IsKey(DIK_W))
	{
		position_ += move;
	}
	if (Input::IsKey(DIK_S))
	{
		position_ -= move;
	}



	//弾が向いてる方向に出すには二点のベクトルを作る
	//弾の発射位置と後ろのベクトル
	//下側にベクトルを作ると上向きに作れる
	//その二つを引くことによって向いてる方向に撃てる

	//攻撃
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//二点のベクトルをプレイヤーに持たせ弾の軌道を修正する

		//発射位置がプレイヤーの動きに合わせてついてくる
		//ベクトルを毎回求める
		D3DXVECTOR3 firePosition = FIRING_POINT;
		D3DXVec3TransformCoord(&firePosition, &firePosition, &m);
		firePosition += position_;

		//発射位置の法線ベクトルを求める
		D3DXVECTOR3 endPosition = END_POINT;
		endPosition += position_;

		//弾の方向と速度を代入
		bulletSpeed_ = firePosition - endPosition;

		//正規化
		//Normalize;

		Bullet* pBullet = CreateGameObject<Bullet>(pParent_);
		pBullet->SetPosition(firePosition);			//発射位置を決める
		pBullet->SetBulletSpeed(bulletSpeed_);		//弾の速度と方向を決める
	}


	//敵をすべて倒したらクリア画面へ
	if (FindObject("Enemy") == nullptr)
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_RESULT);

	}

	

}

//描画
void Player::Draw()
{
	//プレイヤー描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);

	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

}

//開放
void Player::Release()
{
}

//当たり判定の処理
void Player::OnCollision(IGameObject * pTarget)
{
	//壁とぶつかったら
	if (pTarget->GetObjectName() == "Stage")
	{
		//記憶した位置に戻す
		position_ = prePos_;
	}

	//敵とぶつかったら
	if (pTarget->GetObjectName() == "Enemy")
	{
		//プレイヤーだけが消えゲームオーバーシーンへ
		KillMe();
	}

}


