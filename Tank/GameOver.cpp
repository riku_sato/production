#include "GameOver.h"
#include "Engine/GameObject/GameObject.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
GameOver::GameOver(IGameObject * parent)
	: IGameObject(parent, "GameOver"), hPict_(-1)
{
}

//初期化
void GameOver::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/GameOver.jpg");
	assert(hPict_ >= 0);
}

//更新
void GameOver::Update()
{
	//エンターが押されたらプレイシーンに戻る(仮)
	if (Input::IsKeyDown(DIK_RETURN))

	{
		//何らかの処理
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void GameOver::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GameOver::Release()
{
}