#include "Bullet.h"
#include "Player.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"),hModel_(-1),bulletSpeed_(D3DXVECTOR3(0, 0, 0))
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Bullet.fbx");
	assert(hModel_ >= 0);
	//弾の当たり判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.2f);
	AddCollider(collision);

}

//更新
void Bullet::Update()
{

	position_ += bulletSpeed_;

	
	if (position_.z > 50)
	{
		KillMe();
	}

	
}








//描画
void Bullet::Draw()
{
	//バレット描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}




void Bullet::OnCollision(IGameObject * pTarget)
{
	Player* pPlayer = (Player*)FindObject("Player");
	D3DXVECTOR3 speed = pPlayer->GetBulletSpeed();

	//壁に当たったら消える(ここで反射する)
	if (pTarget->GetObjectName() == "Stage")
	{
		//弾の進行ベクトル
		bulletSpeed_ = bulletSpeed_ * 2;
		/////////////壁の法線を求める///////////////
		//1.衝突したときに弾の中心から壁の中心に向かってレイキャストを飛ばす
		//RayCastData data;
		//data.start = position_;					 //レイの発射位置
		//data.dir = pTarget->();					 //レイの方向
		//Model::RayCast(hModel_, &data);			 //レイを発射	

		//2.当たったポリゴンの３頂点から二つのベクトルを求める




		//3,二つのベクトルから外積を求める




		//4,外積が当たった面の箱の法線
	

	}
	//敵と当たったら両方消す	
	if (pTarget->GetObjectName() == "Enemy")
	{
		KillMe();
	}

}

